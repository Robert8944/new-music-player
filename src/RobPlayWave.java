
import java.io.File; 
import java.io.IOException; 
import javax.sound.sampled.AudioFormat; 
import javax.sound.sampled.AudioInputStream; 
import javax.sound.sampled.AudioSystem; 
import javax.sound.sampled.DataLine; 
import javax.sound.sampled.FloatControl; 
import javax.sound.sampled.LineUnavailableException; 
import javax.sound.sampled.SourceDataLine; 
import javax.sound.sampled.UnsupportedAudioFileException; 
 
public class RobPlayWave extends Thread {
    
	private String filename;
    private Position curPosition;
    public byte[] abData;
    public int nBytesRead, songNNum;
    public boolean done = false;
    public FloatControl volumeFPW, pan, sRate;
    private final int EXTERNAL_BUFFER_SIZE = 524288; // 128Kb 
    enum Position {LEFT, RIGHT, NORMAL};
 
    public RobPlayWave(String wavfile, int songnum) { 
        filename = wavfile;
        curPosition = Position.NORMAL;
        songNNum = songnum;
    }
    
    public RobPlayWave(String wavfile, Position p, int songnum) { 
        filename = wavfile;
        curPosition = p;
        songNNum = songnum;
    } 
 
    public void run() { 
    	
    	done = false;
 
        File soundFile = new File(filename);
        if (!soundFile.exists()) { 
            System.err.println("Wave file not found: " + filename);
            return;
        } 
 
        AudioInputStream audioInputStream = null;
        try { 
            audioInputStream = AudioSystem.getAudioInputStream(soundFile);
        } 
        catch (UnsupportedAudioFileException e1) { 
            e1.printStackTrace();
            return;
        }
        catch (IOException e1) { 
            e1.printStackTrace();
            return;
        } 
 
        AudioFormat format = audioInputStream.getFormat();
        SourceDataLine sDL = null;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
        
//Line clip = null;
////				DataLine.Info info = new DataLine.Info(
////				Clip.class, stream.getFormat(), ((int)stream.getFrameLength()*format.getFrameSize()));
//		try {
//			clip = (Clip) AudioSystem.getLine(info);
//		} catch (LineUnavailableException e1) {
//
//			e1.printStackTrace();
//		}
//        
//        FloatControl gainControl =      (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
//		gainControl.setValue((float) 10.0); // Reduce volume by 10 decibels. 
 
        try { 
            sDL = (SourceDataLine) AudioSystem.getLine(info);
            sDL.open(format);
        } 
        catch (LineUnavailableException e) { 
            e.printStackTrace();
            return;
        }
        catch (Exception e) { 
            e.printStackTrace();
            return;
        } 
 
        if (sDL.isControlSupported(FloatControl.Type.PAN)) { 
            pan = (FloatControl) sDL.getControl(FloatControl.Type.PAN);
            if (curPosition == Position.RIGHT) 
                pan.setValue(1.0f);
            else if (curPosition == Position.LEFT) 
                pan.setValue(-1.0f);
        } 
        if (sDL.isControlSupported(FloatControl.Type.MASTER_GAIN)) {      
        	volumeFPW = (FloatControl) sDL.getControl(FloatControl.Type.MASTER_GAIN);  
        	volumeFPW.setValue(MusicPlayer.volumeNum); //6.0206      
        }
//        if (sDL.isControlSupported(FloatControl.Type.VOLUME)) {      
//        	System.out.println("hi");
//        	sRate = (FloatControl) sDL.getControl(FloatControl.Type.VOLUME);  
//        	sRate.setValue(0); //6.0206      
//        }
 
        
        abData = new byte[EXTERNAL_BUFFER_SIZE];
 
        sDL.start();
        
        try { 
            while (nBytesRead != -1) { 
                nBytesRead = audioInputStream.read(abData, 0, abData.length);
                if (nBytesRead >= 0) {
                    sDL.write(abData, 0, nBytesRead);
                }
                //System.out.println(sDL.getFramePosition());//sDL.getMicrosecondPosition()
            } 
        }
        catch (IOException e) { 
            e.printStackTrace();
            return;
        }
        finally { 
        	
            sDL.flush();
            //sDL.stop();
            sDL.close();
            done = true;
        }
        abData = null;
 
    } 
    public String toString(){
    	return filename;
    }
} 
