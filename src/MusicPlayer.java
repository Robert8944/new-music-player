
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MusicPlayer extends JFrame implements ActionListener, ChangeListener{
	private static final long serialVersionUID = 1L;
	private JButton play, pause, next, last, mute, stop, fF, rW;
	private JPanel centerSetUp, bottomSetUp1, bottomSetUp2, rightSetUp, leftSetUp, rightSetUp2;
	private JProgressBar albumProg, songProg, EQ;
	private JTextField songNameDisplay;// songNumText;
	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenuItem addSong, changeLocation, songOptions;
	private JFileChooser fc;
	private JCheckBox repeatOne, repeatAll, shuffle, playAll, playSelected;
	private JSlider volume;
	private JComboBox songSelection;
	private ImageIcon buttonImageMute, buttonImageOn;
	private Timer t;
	private File sorceOfSongs;
	private RobPlayWave playing;
	//private PlayWave playingW;
	private boolean isSongOn, isPaused;
	private ArrayList<String> songNames;
	private HashMap<String, Double> forLengths;
	private String driveName, mainFilePath, startUpOptions;
	private final double KBSOFWAV = 32;//172.2
	private int songNum, numTSongs, secondsPlaying = 0;
	public static int volumeNum;

	public MusicPlayer(boolean resetting) {
		super("Music Player");

		try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());} catch (Exception e) {}
		
		if(!resetting){
			Object[] possibilities = {"In the Standard file location", "Pick a new drive letter for the standard location", "Pick costom location"};
			startUpOptions = (String)JOptionPane.showInputDialog(this,"Where are your music files stored?","Start-Up options",JOptionPane.PLAIN_MESSAGE,null, possibilities,"In the last file location");
		}
		else{
			startUpOptions = "Pick costom location";
		}

		if(startUpOptions==null){
			System.exit(0);
		}

		if(startUpOptions.equals("Pick a new drive letter for the standard location")){
			driveName = JOptionPane.showInputDialog(this, "What is the letter of the dive this program is running on?");
			try{
				while(driveName.length()!=1){
					driveName = JOptionPane.showInputDialog(this, "That was an incorrect drive, name a different drive.");
				}
			}
			catch(Exception ex){
				System.exit(0);
			}
			mainFilePath = driveName+":/compsci/waves/player waves";
			ImageIcon imgIcon = new ImageIcon(driveName+ ":/compsci/Pics/Notes for player.png");
			Image img = imgIcon.getImage();
			this.setIconImage(img);
		}
		else if(startUpOptions.equals("In the Standard file location")){
			char fileChar = 'E';
			mainFilePath = fileChar + ":/compsci/waves";
			ImageIcon imgIcon = new ImageIcon(fileChar + ":/compsci/Pics/Notes for player.png");
			Image img = imgIcon.getImage();
			this.setIconImage(img);
			buttonImageMute = new ImageIcon(fileChar + ":/compsci/Pics/Mute[1].png");
			buttonImageOn = new ImageIcon(fileChar + ":/compsci/Pics/Speaker.png");

		}
		else if(startUpOptions.equals("Pick costom location")){
			//			JFileChooser choosy = new JFileChooser();
			//			choosy.showOpenDialog(this);
			//			if(choosy.getSelectedFile()==null)
			//				System.exit(0);
			//			File selFile = choosy.getSelectedFile();
			//			mainFilePath = selFile.toString();
			mainFilePath = JOptionPane.showInputDialog(this, "What is the direct file path for your music files?(i.e. F:/compsci/waves/player waves {That is the standard file location [be sure to use foreward slashes]}.)");
			if(mainFilePath==null){
				System.exit(0);
			}
		}

		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		addSong = new JMenuItem("Add a Song");
		fileMenu.add(addSong);
		addSong.addActionListener(this);
		changeLocation = new JMenuItem("Change File Location");
		fileMenu.add(changeLocation);
		songOptions = new JMenu("Song Options");
		songOptions.addActionListener(this);
		fileMenu.add(songOptions);
		//songNumText = new JTextField(songNum+"");
		//menuBar.add(songNumText);
		changeLocation.addActionListener(this);
		play = new JButton("Play");
		play.addActionListener(this);
		pause = new JButton("Pause");
		pause.addActionListener(this);
		next = new JButton("Next");
		next.addActionListener(this);
		last = new JButton("Previous");
		last.addActionListener(this);
		stop = new JButton("Stop");
		stop.addActionListener(this);
		mute = new JButton("     Mute", buttonImageOn);
		mute.addActionListener(this);
		fF = new JButton("Fast Forward");
		fF.addActionListener(this);
		rW = new JButton("Rewind");
		rW.addActionListener(this);
		songNameDisplay = new JTextField();
		albumProg = new JProgressBar();
		songProg = new JProgressBar();
		EQ = new JProgressBar(JProgressBar.VERTICAL,0,100);
		centerSetUp = new JPanel();
		centerSetUp.setLayout(new GridLayout(3,1));
		bottomSetUp1 = new JPanel();
		bottomSetUp1.setLayout(new GridLayout(2,1));
		bottomSetUp2 = new JPanel();
		bottomSetUp2.setLayout(new GridLayout(1,2));
		rightSetUp = new JPanel();
		rightSetUp.setLayout(new GridLayout(2,1));
		rightSetUp2 = new JPanel();
		rightSetUp2.setLayout(new GridLayout(1,2));
		leftSetUp = new JPanel();
		leftSetUp.setLayout(new GridLayout(2,1));
		volume = new JSlider(-50,6);
		//volume.setBorder(BorderFactory.createTitledBorder("Volume"));
		volume.addChangeListener(this);
		//volume.setPaintTrack(true);
		t = new Timer(1, this);
		t.start();
		fc = new JFileChooser(new File(""));
		repeatOne = new JCheckBox("Repeat One", false);
		repeatOne.addActionListener(this);
		repeatAll = new JCheckBox("Repeat All", true);
		repeatAll.addActionListener(this);
		playAll = new JCheckBox("Play All", true);
		playAll.addActionListener(this);
		shuffle = new JCheckBox("Shuffle", false);
		shuffle.addActionListener(this);
		playSelected = new JCheckBox("Play Selected Next", false);
		playSelected.addActionListener(this);
		
		songSelection = new JComboBox();
		
		songOptions.add(playSelected);
		songOptions.add(shuffle);
		songOptions.add(repeatOne);
		songOptions.add(repeatAll);
		songOptions.add(playAll);
		menuBar.add(mute);
		menuBar.add(volume);

		songNames = new ArrayList<String>();
		forLengths = new HashMap<String, Double>();
		isSongOn = false;


		sorceOfSongs = new File(mainFilePath);
		for(File f: sorceOfSongs.listFiles()){
			if(this.testMusicFile(f.toString())){
				numTSongs++;
				songNames.add(f.toString());
				forLengths.put(f.toString() , f.length()/KBSOFWAV);
			}
		}



		if(songNames.size()==0){
			JOptionPane.showMessageDialog(this, "there was no music files there...");
			System.exit(0);
		}
		
		for(String s : songNames){
			songSelection.addItem(this.removeExt(new File(s).getName()));
		}
		songSelection.addActionListener(this);

		albumProg.setString(songNum+1+"/"+numTSongs);
		albumProg.setStringPainted(true);
		albumProg.setBorderPainted(true);
		
		bottomSetUp1.add(bottomSetUp2);
		bottomSetUp1.add(songSelection);
		
		bottomSetUp2.add(stop);
		bottomSetUp2.add(pause);

		rightSetUp.add(next);
		rightSetUp.add(fF);
		
		rightSetUp2.add(rightSetUp);
		//rightSetUp2.add(EQ);
		
		leftSetUp.add(last);
		leftSetUp.add(rW);

		centerSetUp.add(albumProg);
		centerSetUp.add(songNameDisplay);
		centerSetUp.add(songProg);

		songNameDisplay.setText(this.removeExt(new File(songNames.get(songNum)).getName()));

		this.add(bottomSetUp1, BorderLayout.SOUTH);
		this.add(play, BorderLayout.NORTH);
		this.add(rightSetUp2, BorderLayout.EAST);
		this.add(leftSetUp, BorderLayout.WEST);
		this.add(centerSetUp, BorderLayout.CENTER);
		this.setJMenuBar(menuBar);

		this.setVisible(true);
		this.setSize(500,180);
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	private String removeExt(String name) {
		return name.substring(0, name.length()-4);
	}
	private boolean testMusicFile(String filePath) {
		if(filePath.substring(filePath.length()-4).equals(".wav"))
			return true;
		//if(filePath.substring(filePath.toCharArray().length-4).equals(".mp3"))
		//return true;
		return false;
	}

	public static void main(String[] args) {new MusicPlayer(false);}

	@SuppressWarnings({ "deprecation" })
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource().equals(play)){
			volumeNum = (volume.getValue());
			if(playing!=null && !playing.toString().equals(songNames.get(songNum))){
				try{playing.stop();isSongOn=false;}catch(Exception ex){}
				if(!isSongOn){
					secondsPlaying = 0;
					isSongOn = true;
					playing = new RobPlayWave(songNames.get(songNum), songNum);
					playing.start();
					pause.setText("Pause");
					isPaused=false;
				}
			}
			else if(isPaused){
				isSongOn=false;
				if(isPaused){
					playing.resume();
				}
				else{
					playing.suspend();
				}
				isPaused=!isPaused;

				if(pause.getText().equals("Pause"))
					pause.setText("Resume");
				else
					pause.setText("Pause");
			}
			
			else if(playing==null){
				try{playing.stop();isSongOn=false;}catch(Exception ex){}
				if(!isSongOn){
					secondsPlaying = 0;
					isSongOn = true;
					playing = new RobPlayWave(songNames.get(songNum), songNum);
					playing.start();
				}
			}
		}
		if(arg0.getSource().equals(pause)){
			isSongOn=false;
			if(isPaused){
				playing.resume();
			}
			else{
				playing.suspend();
			}
			isPaused=!isPaused;

			if(pause.getText().equals("Pause"))
				pause.setText("Resume");
			else
				pause.setText("Pause");
		}
		if(arg0.getSource().equals(stop)){
			try{playing.stop();isSongOn=false;}catch(Exception ex){}
			playing=null;
			pause.setText("Pause");
			isPaused = false;
		}
		if(arg0.getSource().equals(last)){
			//try{playing.stop();isSongOn=false;}catch(Exception ex){}
			if(songNum<=0){
				songNum = numTSongs-1;
			}
			else
				songNum--;
			songNameDisplay.setText(this.removeExt(new File(songNames.get(songNum)).getName()));
			albumProg.setValue((int)(((double)songNum/(double)(numTSongs-1))*100));
			albumProg.setString(songNum+1+"/"+numTSongs);
			try{
				if(!playing.toString().equals(songNames.get(songNum)))
					songProg.setValue(0);
				else 
					songProg.setValue((int)(((double)secondsPlaying/forLengths.get(playing.toString()))*12738.853503184713375796178343949));
			}
			catch(Exception ex){

			}
		}
		if(arg0.getSource().equals(next)){
			//try{playing.stop();isSongOn=false;}catch(Exception ex){}
			if(songNum>=numTSongs-1){
				songNum = 0;
			}
			else
				songNum++;
			songNameDisplay.setText(this.removeExt(new File(songNames.get(songNum)).getName()));
			albumProg.setValue((int)(((double)songNum/(double)(numTSongs-1))*100));
			albumProg.setString(songNum+1+"/"+numTSongs);
			try{
				if(!playing.toString().equals(songNames.get(songNum)))
					songProg.setValue(0);
				else 
					songProg.setValue((int)(((double)secondsPlaying/forLengths.get(playing.toString()))*12738.853503184713375796178343949));
			}
			catch(Exception ex){

			}
		}
		if(arg0.getSource().equals(addSong)){
			fc.showOpenDialog(this);
			if(fc.getSelectedFile()==null)
				return;
			File selFile = fc.getSelectedFile();

			String filePath = selFile.toString();
			if(this.testMusicFile(filePath)){
				try{
					numTSongs++;
					File selFile2 = new File(selFile.toString());
					//selFile2.renameTo(new File(sorceOfSongs, selFile2.getName()));
					songNames.add(selFile2.toString());
				}
				catch(Exception ex){

				}
			}
			else{
				JOptionPane.showMessageDialog(this, "Thats not a .wav or .mp3!");
			}

		}
		if(arg0.getSource().equals(changeLocation)){
			this.dispose();
			new MusicPlayer(true);
		}
		if(arg0.getSource().equals(repeatAll)){
			if(!playAll.isSelected()){
				repeatAll.setSelected(false);
			}
			if(repeatAll.isSelected()){
				repeatOne.setSelected(false);
				playAll.setSelected(true);
			}
		}
		if(arg0.getSource().equals(repeatOne)){
			if(!playAll.isSelected()){
				repeatOne.setSelected(false);
			}
			if(repeatOne.isSelected()){
				repeatAll.setSelected(false);
				playAll.setSelected(true);
			}
		}
		if(arg0.getSource().equals(playAll)){
			if(!playAll.isSelected()){
				repeatOne.setSelected(false);
				repeatAll.setSelected(false);
				shuffle.setSelected(false);
				
			}
			else{
				
			}
		}
		if(arg0.getSource().equals(shuffle)){
			if(!playAll.isSelected()){
				shuffle.setSelected(false);
			}
			else{
				
			}
		}
		if(arg0.getSource().equals(playSelected)){
			if(playSelected.isSelected()){
				repeatAll.setSelected(false);
				shuffle.setSelected(false);
				playAll.setSelected(false);
			}
		}
		if(arg0.getSource().equals(songSelection)){
			songNum = songSelection.getSelectedIndex()-1;
			this.actionPerformed(new ActionEvent(next,0,null));
			//this.actionPerformed(new ActionEvent(play,0,null));
		}
		if(arg0.getSource().equals(t)){
			if(playing!=null){
				//playing.volumeFPW.setValue(volume.getValue());
				if(playing.done==true && playAll.isSelected()){
					if(repeatAll.isSelected()){
						songNum = playing.songNNum;
						if(shuffle.isSelected())
							songNum = (int)(Math.random()*numTSongs);
						this.actionPerformed(new ActionEvent(next,0,null));
						this.actionPerformed(new ActionEvent(play,0,null));
					}
					else if(repeatOne.isSelected()){
						songNum = playing.songNNum;
						this.actionPerformed(new ActionEvent(play,0,null));
					}
					else if(playSelected.isSelected()){
						this.actionPerformed(new ActionEvent(stop,0,null));
						this.actionPerformed(new ActionEvent(play,0,null));
					}
					else{
						if(songNum!=numTSongs){
							songNum = playing.songNNum;
							this.actionPerformed(new ActionEvent(next,0,null));
							this.actionPerformed(new ActionEvent(play,0,null));
						}
					}
				}
				if(playing.sRate!=null){
					EQ.setValue((int)((playing.sRate.getValue()*100) / playing.sRate.getMaximum()));
					System.out.println(playing.sRate.getValue());
				}
			}
			
			//try{
			//if(Integer.parseInt(songNumText.getText())<=numTSongs){
			//songNum = (Integer.parseInt(songNumText.getText()));
			//this.actionPerformed(new ActionEvent(next,0,null));
			//}
			//}catch(Exception ex){}

		}
		if(arg0.getSource().equals(mute)){
			if(mute.getText().equals("     Mute")){
				mute.setText("UnMute");
				mute.setIcon(buttonImageMute);
				volumeNum = (volume.getValue());
				if(playing!=null){
					playing.volumeFPW.setValue( -10000F);
				}
			}
			else{
				mute.setText("     Mute");
				mute.setIcon(buttonImageOn);
				volumeNum = (volume.getValue());
				if(playing!=null){
					playing.volumeFPW.setValue(volume.getValue());
				}
			}
		}
	}
	
	public void stateChanged(ChangeEvent arg0) {
		if(arg0.getSource().equals(volume)){
			if(mute.getText().equals("UnMute")){
				mute.setText("     Mute");
				mute.setIcon(buttonImageOn);
			}
			volumeNum = (volume.getValue());
			if(playing!=null){
				//System.out.println(volume.getValue());
				playing.volumeFPW.setValue(volume.getValue());
			}
		}

	}

}
